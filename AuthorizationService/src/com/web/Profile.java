package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.domain.Adress;
import com.domain.User;
import com.storage.StorageService;

public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
	
		
		StorageService service = new StorageService();
		
		service.connect();
		
		String username = request.getSession().getAttribute("login").toString();
		
		User u = service.getProfile(request.getSession().getAttribute("login").toString());
	
		
		
		PrintWriter out = response.getWriter();
		
		out.println("<html><body>" +
					"<h2>Your Profile</h2>" + 
					"<p>Username: " + u.getUsername() + "</p>" +
					"<p>Password: " + u.getPassword() + "</p>" +
					"<p>Email: " + u.getEmail() + "</p>" +
					"<p>Account Type: " + u.getType() + "</p>" +
					"<p><a href='index.jsp'>Main Page</a>****<a href='logout'>Logout</a></p>" +
					"<p>***********************************</p>");
		
		for (Adress adress : service.getAdresses(username)) {
			  out.println("<p>Type: " + adress.getType() + "</p>" + 
					  "<p>Voivodeship: " + adress.getVoivodeship() + "</p>" +
					  "<p>City: " + adress.getCity() + "</p>" +
					  "<p>Zip Code: " + adress.getZipcode() + "</p>" +
					  "<p>Street: " + adress.getStreet() + "</p>" +
					  "<p>No of House/Appartement: " + adress.getNumber() + "</p>" +
					  "<p><a href='editadressform'>Edit</a>****<a href='deleteadressform'>Delete</a></p>" +
					  "<p>---------------------------------</p>");
		  }
		
		out.println("<p><a href='adressform'>Add Adress</a></p>");
	}

}