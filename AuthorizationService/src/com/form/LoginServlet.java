package com.form;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.storage.StorageService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = "/FormServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/loginForm.jsp");
        requestDispatcher.forward(req, resp);
    }
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)  
	        throws ServletException, IOException {  
	  
	    response.setContentType("text/html");  
	    PrintWriter out = response.getWriter();  
	          
	    String n=request.getParameter("username");  
	    String p=request.getParameter("password");  
	          
	    if(StorageService.validate(n, p)){  
	        RequestDispatcher rd=request.getRequestDispatcher("servlet2");  
	        rd.forward(request,response);  
	    }  
	    else{  
	        out.print("Username or password error");  
	        RequestDispatcher rd=request.getRequestDispatcher("loginForm.jsp");  
	        rd.include(request,response);  
	    }  
	          
	    out.close();  
	    }  


}
