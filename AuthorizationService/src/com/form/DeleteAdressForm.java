package com.form;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DeleteAdressForm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		out.println("<h2>Delete an Adress</h2>" +
				"<form action='deleteadress'>" +
				"What Adress do you want to delete ?: <select name='type'>" +
				"<option>Work</option>" + 
				"<option>Home</option>" +
				"<option>Of Correspondence</option>" + 
				"</select> <br />" +
				"<input type='submit' value=' DELETE ' />" +
				"</form>");
	}

}